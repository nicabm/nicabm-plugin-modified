<?php
/**
 * Custom Post Types Configuration
 *
 * @package     NICABM\Utility\CustomPostTypes
 * @since       0.1.0
 * @author      Tim Jensen
 * @link        https://nicabm.com/
 * @license     GNU General Public License 2.0+
 */

namespace NICABM\Utility;

/**
 * The configuration array for registering custom post types.
 * The args array follows the same format as the Codex except for labels,
 * which are auto generated based upon the values for 'singular' and 'plural'
 *
 * @see https://codex.wordpress.org/Function_Reference/register_post_type
 */
return [
	[
		'post_type' => 'nicabm_course',
		'args'      => [
			'labels'            => [
				'singular'  => 'Course',
				'plural'    => 'Courses',
				'menu_name' => 'Courses',
			],
			'public'            => true,
			'supports'          => [
				'title',
				'revisions',
			],
			'has_archive'       => false,
			'rewrite'           => [
				'slug' => 'course',
			],
			'menu_icon'         => 'dashicons-book-alt',
			'show_in_rest'      => true,
			'menu_position'     => 25,
			'title_placeholder' => 'Enter Course title here',
		],
	],
	[
		'post_type' => 'nicabm_program',
		'args'      => [
			'labels'            => [
				'singular'  => 'Program',
				'plural'    => 'Programs',
				'menu_name' => 'Programs',
			],
			'public'            => true,
			'supports'          => [
				'editor',
				'genesis-scripts',
				'genesis-seo',
				'page-attributes',
				'revisions',
				'thumbnail',
				'title',
			],
			'has_archive'       => false,
			'rewrite'           => [
				'slug' => 'program',
			],
			'menu_icon'         => 'dashicons-welcome-learn-more',
			'show_in_rest'      => true,
			'menu_position'     => 25,
			'title_placeholder' => 'Enter Program name here',
		],
	],
	[
		'post_type' => 'nicabm_offer',
		'args'      => [
			'labels'            => [
				'singular'  => 'Offer',
				'plural'    => 'Offers',
				'menu_name' => 'Offers',
			],
			'public'            => true,
			'supports'          => [
				'editor',
				'genesis-scripts',
				'genesis-seo',
				'page-attributes',
				'revisions',
				'thumbnail',
				'title',
			],
			'has_archive'       => false,
			'rewrite'           => [
				'slug' => 'offer',
			],
			'menu_icon'         => 'dashicons-migrate',
			'show_in_rest'      => true,
			'menu_position'     => 25,
			'title_placeholder' => 'Enter Offer title here',
		],
	],
	[
		'post_type' => 'nicabm_instructor',
		'args'      => [
			'labels'            => [
				'singular'  => 'Instructor',
				'plural'    => 'Instructors',
				'menu_name' => 'Instructors',
			],
			'public'            => true,
			'supports'          => [
				'title',
				'editor',
				'thumbnail',
				'revisions',
			],
			'has_archive'       => false,
			'rewrite'           => [
				'slug' => 'instructor',
			],
			'menu_icon'         => 'dashicons-businessman',
			'show_in_rest'      => true,
			'menu_position'     => 25,
			'title_placeholder' => 'Enter Instructor name here',
		],
	],
	[
		'post_type' => 'nicabm_testimonial',
		'args'      => [
			'labels'            => [
				'singular'  => 'Testimonial',
				'plural'    => 'Testimonials',
				'menu_name' => 'Testimonials',
			],
			'public'            => true,
			'supports'          => [
				'title',
				'editor',
				'thumbnail',
				'revisions',
			],
			'has_archive'       => false,
			'rewrite'           => [
				'slug' => 'testimonial',
			],
			'menu_icon'         => 'dashicons-thumbs-up',
			'show_in_rest'      => true,
			'menu_position'     => 25,
			'title_placeholder' => 'Enter Testimonial headline here',
		],
	],
[
	'post_type' => 'nicabm_cecme',
	'args'      => [
	  'labels'            => [
		'singular'  => 'Cecme',
		'plural'    => 'Cecmes',
		'menu_name' => 'Cecmes',
	  ],
	  'public'            => true,
	  'supports'          => [
		'title',
		'editor',
		'thumbnail',
		'revisions',
	  ],
	  'has_archive'       => false,
	  'rewrite'           => [
		'slug' => 'cecme',
	  ],
	  'menu_icon'         => 'dashicons-awards',
	  'show_in_rest'      => true,
	  'menu_position'     => 25,
	  'title_placeholder' => 'Enter Cecme name here',
		],
	],
[
	  'post_type' => 'nicabm_confirm',
	  'args'      => [
		'labels'            => [
		  'singular'  => 'Confirm Page',
		  'plural'    => 'Confirm Pages',
		  'menu_name' => 'Confirm Pages',
		],
		'public'            => true,
		'supports'          => [
		  'editor',
		  'genesis-scripts',
		  'genesis-seo',
		  'page-attributes',
		  'revisions',
		  'thumbnail',
		  'title',
		],
		'has_archive'       => false,
		'rewrite'           => [
		  'slug' => 'confirm',
		],
		'menu_icon'         => 'dashicons-welcome-add-page',
		'show_in_rest'      => true,
		'menu_position'     => 25,
		'title_placeholder' => 'Enter Confirm Page name here',
		],
	],
	[
	'post_type' => 'nicabm_order',
	'args'      => [
	'labels'            => [
		'singular'  => 'Order Page',
		'plural'    => 'Order Pages',
		'menu_name' => 'Order Pages',
	],
	'public'            => true,
	'supports'          => [
		'editor',
		'genesis-scripts',
		'genesis-seo',
		'genesis-layouts',
		'page-attributes',
		'revisions',
		'thumbnail',
		'title',
	],
	'has_archive'       => false,
	'rewrite'           => [
		'slug' => 'order',
	],
	'menu_icon'         => 'dashicons-cart',
	'show_in_rest'      => true,
	'menu_position'     => 25,
	'title_placeholder' => 'Enter Order Page name here',
		],
	],
	[
	'post_type' => 'nicabm_samples',
	'args'      => [
	'labels'            => [
		'singular'  => 'Sample',
		'plural'    => 'Samples',
		'menu_name' => 'Samples',
	],
	'public'            => true,
	'supports'          => [
		'editor',
		'genesis-scripts',
		'genesis-seo',
		'genesis-layouts',
		'page-attributes',
		'revisions',
		'thumbnail',
		'title',
	],
	'has_archive'       => false,
	'rewrite'           => [
		'slug' => 'sample',
	],
	'menu_icon'         => 'dashicons-carrot',
	'show_in_rest'      => true,
	'menu_position'     => 25,
	'title_placeholder' => 'Enter Sample Page name here',
		],
	],
];
